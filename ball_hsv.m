%% BALL_HSV - a script for getting HSV information
% This script can be used to determine the hue, saturation and value
% thresholds for the new multi-color detector. 

%% Load image
fname = input('Enter the name of the image file (with extension): ', 's');
RGB = imread(fname);

%% Mark center and radius of a ball
figure(1)
clf
imshow(RGB)

f = msgbox(sprintf('Use the first point to mark the center. Then, use the second point to mark radius.'),'Select center and radius');
while isvalid(f)
    pause(0.05)
end
[x_c, y_c] = ginput(1);
hold on
scatter(x_c,y_c,50,'kx')
[x_r, y_r] = ginput(1);
r = ceil(sqrt((x_c-x_r)^2 + (y_c-y_r)^2));

%% Fine tune the center and radius
cut_correct = false;
RGB_cut = RGB((y_c-r):(y_c+r), (x_c-r):(x_c+r), :);
figure(2)
clf
C = imshow(RGB_cut);
while ~cut_correct    
    x_shift = round(input('Enter positive number to move the cut to the right, negative number to move it to the left, or zero to keep it: '));
    if x_shift ~= 0
        x_c = x_c + x_shift;
        RGB_cut = RGB((y_c-r):(y_c+r), (x_c-r):(x_c+r), :);
        C.CData = RGB_cut;
    end
        
    y_shift = round(input('Enter positive number to move the cut down, negative number to move it up, or zero to keep it: '));
    if x_shift ~= 0
        y_c = y_c + y_shift;
        RGB_cut = RGB((y_c-r):(y_c+r), (x_c-r):(x_c+r), :);
        C.CData = RGB_cut;
    end
    
    r_shift = round(input('Enter positive number to increase the radius, negative number to decrease it, or zero to keep it: '));
    if r_shift ~= 0
        r = r + r_shift;
        RGB_cut = RGB((y_c-r):(y_c+r), (x_c-r):(x_c+r), :);
        C.CData = RGB_cut;
    end
    
    if lower(input('Is the cut-out correct (y/n)?', 's')) == 'y'
        cut_correct = true;
    end
end

%% Show HSV distribution of the cut-out
[x_coord, y_coord] = meshgrid(-r:r,-r:r);
dist = sqrt(x_coord.^2 + y_coord.^2);
in_circle = dist <= r;

circle_R = RGB_cut(:,:,1);
circle_R = circle_R(in_circle);
circle_G = RGB_cut(:,:,2);
circle_G = circle_G(in_circle);
circle_B = RGB_cut(:,:,3);
circle_B = circle_B(in_circle);
circle_pixels = reshape([circle_R, circle_G, circle_B], numel(circle_R), 1, 3);
HSV_cut = rgb2hsv(circle_pixels);

figure(3)
clf
subplot(1,3,1)
histogram(HSV_cut(:,:,1) * 360)
xlabel('Hue [�]')
ylabel('Frequency')
title('Hue')

subplot(1,3,2)
histogram(HSV_cut(:,:,1) * 100)
xlabel('Saturation [%]')
ylabel('Frequency')
title('Saturation')

subplot(1,3,3)
histogram(HSV_cut(:,:,1) * 100)
xlabel('Value [%]')
ylabel('Frequency')
title('Value')

%% Test HSV thresholding on the full image
HSV = rgb2hsv(RGB);
h_center = mod(input('Enter middle hue (in degrees): '), 360) / 360;
h_range = mod(input('Enter hue range (in degrees): '), 360) / 360;
s_min = input('Enter minimum saturation (in percent): ') / 100;
v_min = input('Enter minimum value (in percent): ') / 100;

hue_diff = mod(HSV(:,:,1) - h_center, 1.0);
hue_threshold = (hue_diff <= h_range) | (hue_diff >= 1 - h_range);
threshold = hue_threshold & (HSV(:,:,2) >= s_min) & (HSV(:,:,3) >= v_min);

figure(4)
clf
imshow(threshold)
title('Thresholded image')