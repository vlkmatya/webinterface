#!/usr/bin/env python3

from eventlet.green import socket
import struct
import logging
import subprocess
from threading import Lock, Thread
from flask import Flask, render_template, make_response, Response, jsonify, redirect, url_for
from flask_socketio import SocketIO, emit
from PIL import Image
import numpy as np
from enum import IntEnum
import requests
import time
import cv2
import click
from calib import CalibrationStatus
import urllib.request
import saver

# application runs on a web server (localhost:5000) using Flask
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
# I/O socket for communication between browser and app
socketio = SocketIO(app, async_mode="threading")

# thread for receiving data from Simulink model
thread = None
thread_lock = Lock()

# socket for communication with Simulink model
simulink_socket = None

# number of objects
NUM_BALLS = 3
MAX_NUM_TRIANGLES = 3

TRIANGLE_HEIGHT = 18

DETECTOR_NAME = "Triangle"

CALIB_CENTER_PIXELS = 20
y1,x1 = np.meshgrid([35,15,-5,-25],[35,15,-5,-25])
y1 = np.reshape(y1, (16,1))
x1 = np.reshape(x1, (16,1))
y2,x2 = np.meshgrid([25,5,-15,-35],[25,5,-15,-35])
y2 = np.reshape(y2, (16,1))
x2 = np.reshape(x2, (16,1))
CALIB_TEST_POINTS = np.concatenate((np.concatenate((x1,x2), 0), np.concatenate((y1,y2), 0)), 1)

TRANSDUCER_HEIGHT = 0.078

H_UNROT_FILENAME = '/home/pi/H_unrot.bin'
CORRECTION_FILENAME = '/home/pi/correction.bin'
HEIGHT_FILENAME = '/home/pi/height.bin'

# size of the manipulation area
area_size = 100.0

# enumeration of platform modes
class ModeEnum(IntEnum):
    NONE = 0
    BALL = 1
    TRIANGLE2 = 2
    TRIANGLE3 = 3

# platform status
class Status:
    def __init__(self):
        self.mode = ModeEnum.NONE
        self.triangle_position = [[0.0, 0.0, 0.0] for i in range(MAX_NUM_TRIANGLES)]
        self.triangle_reference = [[25*np.cos(a), 25*np.sin(a), a] for a in np.linspace(0, 2*np.pi, num=MAX_NUM_TRIANGLES, endpoint=False)]
        self.ball_position = [[0.0, 0.0] for i in range(NUM_BALLS)]
        self.ball_reference = [[25*np.cos(a), 25*np.sin(a)] for a in np.linspace(0, 2*np.pi, num=NUM_BALLS, endpoint=False)]
        self.demo = 0

status = Status()

calib_status = CalibrationStatus(CALIB_TEST_POINTS)

# receives data from Simulink and sends them to web
# Simulink sends position of the tracked ball every control period
def readPosThread():
    global simulink_socket
    simulink_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    simulink_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    simulink_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    simulink_socket.bind(('',25001))

    print('start service ...')

    ball_bytes = NUM_BALLS * 8
    triangle_bytes = MAX_NUM_TRIANGLES * 12
    message_bytes = np.max([ball_bytes, triangle_bytes])

    while True :
        message,address = simulink_socket.recvfrom(message_bytes)
        if status.mode == ModeEnum.BALL: 
            try:           
                data = struct.unpack("=" + str(NUM_BALLS*2) + "f", message)
                status.ball_position = [[data[2*i], data[2*i+1]] for i in range(NUM_BALLS)]
            except:
                print("Received partial/incorrect packet")
        elif status.mode == ModeEnum.TRIANGLE2:
            try:
                data = struct.unpack("=" + str(6) + "f", message)
                for i in range(2):
                    status.triangle_position[i] = [data[3*i], data[3*i+1], data[3*i+2]]
            except:
                print("Received partial/incorrect packet")
        elif status.mode == ModeEnum.TRIANGLE3:
            try:
                data = struct.unpack("=" + str(9) + "f", message)
                for i in range(3):
                    status.triangle_position[i] = [data[3*i], data[3*i+1], data[3*i+2]]
            except:
                print("Received partial/incorrect packet")
        web_update()

def get_my_ip():
    return subprocess.check_output(['hostname', '--all-ip-addresses']).decode().split()

def responseImage(image):
    response = make_response(image)
    response.headers['Content-Type'] = 'image/png'
    return response

# function for updating web interface
def web_update():
    if status.mode == ModeEnum.BALL:
        socketio.emit('update', {'position': status.ball_position, 'reference': status.ball_reference}, broadcast = True)
    elif status.mode in (ModeEnum.TRIANGLE2, ModeEnum.TRIANGLE3):
        socketio.emit('update', {'position': status.triangle_position, 'reference': status.triangle_reference}, broadcast = True)

def get_current_detector():
    name = 'None'
    try:
        cfg = requests.get('http://localhost:5001/config')
        name = cfg.json()['detectors'][0]['name']
    except:
        name = 'Service offline'
    return name

@socketio.on('get_detector')
def request_detector():
    socketio.emit('detector', get_current_detector())

@socketio.on('change_detector')
def change_detector(filename):
    try:
        requests.post('http://localhost:5001/config/loadfile', filename)
    except:
        print('Could not load configuration from "{}". Computer vision is probably offline.'.format(filename))

def simulink_update():
    data = bytearray()
    data.extend(struct.pack("=b", status.demo))
    if status.mode == ModeEnum.BALL:
        for i in range(NUM_BALLS):
            ref = status.ball_reference[i]
            data.extend(struct.pack("=ff", ref[0], ref[1]))
    elif status.mode == ModeEnum.TRIANGLE2:
        for i in range(2):
            ref = status.triangle_reference[i]
            data.extend(struct.pack("=fff", ref[0], ref[1], ref[2]))
    elif status.mode == ModeEnum.TRIANGLE3:
        for i in range(2):
            ref = status.triangle_reference[i]
            data.extend(struct.pack("=fff", ref[0], ref[1], ref[2]))
    simulink_socket.sendto(data, ("127.0.0.1",25000))
    
@socketio.on('connect')
def onconnect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=readPosThread)

# responds to the web interface requesting update
@socketio.on('request_update')
def request_update():
    web_update()

@socketio.on('set_demo')
def set_demo(d):
    #print("Demo set to {}".format(d))
    status.demo = int(d)
    simulink_update()

# responds to changing position of the reference in web interface
@socketio.on('reference_center')
def reference_center(id, position):
    #print("Object {} moved to {}".format(id,position))
    status.demo = 0
    status.triangle_reference[id][0] = position[0]
    status.triangle_reference[id][1] = position[1]
    web_update()
    simulink_update()

@socketio.on('reference_end')
def reference_end(id, position):
    #print("Object {} moved to {}".format(id,position))
    status.demo = 0
    x_diff = position[0] - status.triangle_reference[id][0]
    y_diff = position[1] - status.triangle_reference[id][1]
    status.triangle_reference[id][2] = np.arctan2(y_diff, x_diff)
    distance_ratio = 2.0 * TRIANGLE_HEIGHT / np.sqrt(x_diff**2 + y_diff**2) / 3.0
    status.triangle_reference[id][0] = position[0] - x_diff*distance_ratio
    status.triangle_reference[id][1] = position[1] - y_diff*distance_ratio
    web_update()
    simulink_update()

@socketio.on('ball_reference')
def ball_reference(id, position):
    #print("Object {} moved to {}".format(id,position))
    status.demo = 0
    status.ball_reference[id][0] = position[0]
    status.ball_reference[id][1] = position[1]
    web_update()
    simulink_update()

@socketio.on('log')
def log(*args):
    print(*args)

@socketio.on('height')
def calib_height(height):
    calib_status.calculate_correction(float(height))

@socketio.on('corner')
def calib_corner(id, position):
    #print('Corner {} set to {}'.format(id, position))
    calib_status.corners[id,:] = position

@socketio.on('center')
def calib_center(id, position):
    #print('Center {} set to {}'.format(id, position))
    calib_status.centers[id,:] = position

@app.route('/<service>/<command>')
def systemd(service, command):
    if not service in ('triangles2', 'triangles3', 'RGBballs', 'vision'):
        raise RuntimeError("Unallowed service {}".format(service))
    if not command in ('start', 'stop', 'status', 'restart'):
        raise RuntimeError("Unallowed command {}".format(command))

    with subprocess.Popen(["systemctl", command, service], stdout=subprocess.PIPE) as proc:
        if service in ('triangles2', 'triangles3', 'RGBballs') and command in ('start','restart'):
            time.sleep(0.5)
        return Response(proc.stdout.read().decode(), mimetype='text/plain')

@app.route('/calib/<int:step>')
def calib_page(step):
    ip = get_my_ip()
    kwargs = {'my_ip':ip[0]}
    if step == 0:
        systemd('RGBballs','stop')
        systemd('triangles2','stop')
        systemd('triangles3','stop')
    elif step == 5:
        calib_status.homography_from_centers()
        # generate transducer coordinates
        y,x = np.meshgrid(np.linspace(35,-35,8), np.linspace(35,-35,8))
        y = np.reshape(y, (64,1))
        x = np.reshape(x, (64,1))
        points = np.concatenate((x,y), 1)
        points_transformed = CalibrationStatus.inverse_transform(points.T, calib_status.H_centers)
        kwargs['points'] = points_transformed.T
    elif step == 6:
        saver.saveMatrix(calib_status.H_centers, H_UNROT_FILENAME)
        saver.saveScalar(calib_status.correction, CORRECTION_FILENAME)
        saver.saveScalar(TRANSDUCER_HEIGHT, HEIGHT_FILENAME)
    return render_template("calib/{}.html".format(step), **kwargs)

@app.route('/calib/4/<int:center>')
def calib_centers(center):
    if center == 0:
        calib_status.homography_from_corners()
    transducer_x = CALIB_TEST_POINTS[center, 0]
    transducer_y = CALIB_TEST_POINTS[center, 1]
    #print('Step {}, x={}, y={}'.format(center, transducer_x, transducer_y))
    img_coordinates = np.round(CalibrationStatus.inverse_transform(np.array([transducer_x,transducer_y]), calib_status.H_corners))
    x = int(img_coordinates[0])
    y = int(img_coordinates[1])
    ip = get_my_ip()
    return render_template("calib/4.html", my_ip=ip[0], center=center, x=x, y=y)

@app.route('/poweroff')
def poweroff():
    subprocess.call(["poweroff"])
    return "OK"

@app.route('/reboot')
def reboot():
    subprocess.call(["reboot"])
    return "OK"

@app.route('/ip')
def list_ip():
    return jsonify(get_my_ip())

@app.route('/balls')
def balls():
    systemd('triangles2', 'stop')
    systemd('triangles3', 'stop')
    change_detector('/home/pi/config_balls.json')
    status.mode = ModeEnum.BALL
    status.demo = 0
    time.sleep(2.0)
    systemd('RGBballs', 'start')
    #time.sleep(0.5)
    simulink_update()
    return render_template("balls.html", area_size=area_size)

@app.route('/triangle2')
def triangle2():
    systemd('RGBballs', 'stop')
    systemd('triangles3', 'stop')
    change_detector('/home/pi/config_triangle.json')
    status.mode = ModeEnum.TRIANGLE2
    status.demo = 0
    time.sleep(2.0)
    systemd('triangles2', 'start')
    #time.sleep(0.5)
    simulink_update()
    return render_template("triangle.html", area_size=area_size, num_obj=2)

@app.route('/triangle3')
def triangle3():
    systemd('RGBballs', 'stop')
    systemd('triangles2', 'stop')
    change_detector('/home/pi/config_triangle.json')
    status.mode = ModeEnum.TRIANGLE3
    status.demo = 0
    time.sleep(2.0)
    systemd('triangles3', 'start')
    #time.sleep(0.5)
    simulink_update()
    return render_template("triangle.html", area_size=area_size, num_obj=3)

@app.route('/')
def index():
    status.mode = ModeEnum.NONE
    systemd('triangles2', 'stop')
    systemd('triangles3', 'stop')
    systemd('RGBballs', 'stop')
    return render_template("index.html")

@click.command()
@click.option('--area-size', type=click.FLOAT, default=100, help="Size of the manipulation area that is shown in the webpage")
def main(**kwargs):
    global area_size
    area_size = kwargs["area_size"]
    global app_running
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    socketio.run(app, "::", debug=True, use_reloader=False)

if __name__ == '__main__':
    main()
