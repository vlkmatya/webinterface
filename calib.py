import numpy as np
import cv2

# calibration constants (in millimeters)
PLEXI_THICKNESS = 5.0
CAMERA_LENS_HEIGHT = 26.7
TRANSDUCER_DISTANCE = 87

# data container for calibration
class CalibrationStatus:
	def __init__(self, calib_pattern):
		self.corners = np.array([[0.0, 0.0] for i in range(4)])
		self.H_corners = np.eye(3)
		self.centers = np.array([[0.0, 0.0] for i in range(calib_pattern.shape[0])])
		self.H_centers = np.eye(3)
		self.correction = 1.0
		self.calib_pattern = calib_pattern

	def sort_corners(self):
		x_ind = np.argsort(self.corners[:,0])
		self.corners = self.corners[x_ind,:]
		corners_column = self.corners[0:1,:]
		y_ind = np.argsort(corners_column[:,1])
		self.corners[0:1,:] = corners_column[y_ind,:]
		corners_column = self.corners[2:3,:]
		y_ind = np.argsort(corners_column[:,1])
		self.corners[2:3,:] = corners_column[y_ind,:]

	def calculate_correction(self, height):
		self.correction = (height + PLEXI_THICKNESS - CAMERA_LENS_HEIGHT) / (height + PLEXI_THICKNESS - CAMERA_LENS_HEIGHT + TRANSDUCER_DISTANCE)
		print('Correction: {}'.format(self.correction))

	def homography_from_corners(self):
		self.sort_corners()
		corners_actual = np.array([[35,35],[35,-35],[-35,35],[-35,-35]])
		H, _ = cv2.findHomography(self.corners, corners_actual)
		print('H = {}'.format(H))
		if not H is None:
			self.H_corners = H

	def homography_from_centers(self):
		H, _ = cv2.findHomography(self.centers, self.calib_pattern)
		print('H = {}'.format(H))
		if not H is None:
			self.H_centers = H

	@staticmethod
	def transform(points, H, correction=1.0):
		p_shape = points.shape
		n_dims = len(p_shape)
		if n_dims == 1:
			assert p_shape[0] == 2, 'Incorrect point dimension.'
			points = np.reshape(points, (2,1))
		elif n_dims == 2:
			assert p_shape[0] == 2, 'N points must be entered as a 2-by-N matrix.'
		else:
			raise ValueError('Argument points has incorrect shape.')
		n_points = points.shape[1]
		points_transformed = np.zeros((2,n_points))		
		for i in range(n_points):
			hom_coords = H @ np.concatenate((points[:,i], [[1.0]]), 0)
			euclid_coords = hom_coords[0:1] / hom_coords[2] * correction
			points_transformed[:, i] = euclid_coords
		return points_transformed

	@staticmethod
	def inverse_transform(points, H, correction=1.0):
		p_shape = points.shape
		n_dims = len(p_shape)
		if n_dims == 1:
			assert p_shape[0] == 2, 'Incorrect point dimension.'
			points = np.reshape(points, (2,1))
		elif n_dims == 2:
			assert p_shape[0] == 2, 'N points must be entered as a 2-by-N matrix.'
		else:
			raise ValueError('Argument points has incorrect shape.')
		n_points = points.shape[1]
		points_transformed = np.zeros((2,n_points))		
		for i in range(n_points):
			coeffs = np.linalg.solve(H, np.concatenate((points[:,i].reshape((2,1)), [[correction]]), 0))
			points_transformed[0, i] = coeffs[0] / coeffs[2]
			points_transformed[1, i] = coeffs[1] / coeffs[2]
		return points_transformed
